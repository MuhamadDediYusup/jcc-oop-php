<?php

class Ape extends Animal
{
    public $legs;

    public function get_legs()
    {
        return $this->legs = 2;
    }

    public function yell()
    {
        return "Auooo";
    }
}
