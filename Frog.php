<?php

class Frog extends Animal
{
    public function jump()
    {
        return "hop hop";
    }
}


$kodok = new Frog("buduk");
$kodok->jump(); // "hop hop"
